package homework.lambdas;

import java.util.*;
import java.util.stream.Collectors;

public class LambdasMain {
   public static void main(String[] args) {
      List<String> list = new ArrayList<>();
      list.add("fdhfdh");
      list.add("fbnodinba");
      list.add("");
      list.add("Fwoiuvxcvb15");
      list.add("alkg lo");
      list.add("");
      list.add("Tdfbwegqwedhbvfdh");
      list.add("aoskpb");
      list.add("UUjvnlkmg");
      list.add("nw ,mv bmvm,a");

//       Zadanie 10
//       list.forEach(value -> System.out.println(value));

//       Zadanie 11
//       System.out.println("items length:");
//       itemsLength(list);
//       System.out.println("sum of lengths:");
//       sumOfLenghts(list);
//       System.out.println("shortest length:");
//       shortestLength(list);
//       System.out.println("longest length:");
//       longestLength(list);

//       Zadanie 12
//       System.out.println("items which start with uppercase:");   //nie gotowe
//       startsWithUpperCase(list);
//       System.out.println("items which start with 'a':");
//       startsWithA(list);
//       System.out.println("items which end with 'a':");
//       endsWithA(list);

//       Zadanie 13
//      System.out.println("sorted list:");
//      sortedList(list);
//      System.out.println();
//      System.out.println("list ordered by length - increasing:");
//      orderShortToLong(list);
//      System.out.println();
//      System.out.println("list ordered by length - decreasing:");
//      orderLongToShort(list);
//      System.out.println();
//      System.out.println("number of signs sorted:");
//      orderLength(list);

//       Zadanie 14
      listToSet(list);
      System.out.println();
      listToNewList(list);


   }

   public static void itemsLength(List<String> list) {
      list.stream().forEach(item -> System.out.println(item.length()));
   }

   public static void sumOfLenghts(List<String> list) {
      System.out.println(list.stream()
              .mapToInt(item -> item.length())
              .sum());
   }

   public static void shortestLength(List<String> list) {
      System.out.println(list.stream()
              .mapToInt(item -> item.length())
              .min()
              .orElse(-1));
   }

   public static void longestLength(List<String> list) {
      System.out.println(list.stream()
              .mapToInt(item -> item.length())
              .max()
              .orElse(-1));
   }

   public static void startsWithUpperCase(List<String> list) {

   }

   public static void startsWithA(List<String> list) {
      list.stream()
              .filter(item -> item.startsWith("a"))
              .forEach(System.out::println);
   }

   public static void endsWithA(List<String> list) {
      list.stream()
              .filter(item -> item.endsWith("a"))
              .forEach(System.out::println);
   }

   public static void sortedList(List<String> list){
      list.sort(String::compareToIgnoreCase);
      list.forEach(System.out::println);

   }

   public static void orderShortToLong(List<String> list) {
      list.stream()
              .sorted(Comparator.comparing(String::length))
              .forEach(System.out::println);
   }

   public static void orderLongToShort(List<String> list) {
      list.stream()
              .sorted(Comparator.comparing(String::length).reversed())
              .forEach(System.out::println);
   }

   public static void orderLength(List<String> list){
      list.stream()
              .filter(item -> !item.isEmpty())
              .sorted(Comparator.comparing(String::length))
              .forEach(item -> System.out.println(item.length()));
   }

   public static void listToSet(List<String> list){
      Set<String> set = new TreeSet<>();
      list.stream()
              .filter(item -> !item.isEmpty())
              .forEach(item -> set.add(item));

      set.forEach(System.out::println);
   }

   public static void listToNewList(List<String> list){
      List<String> sortedList = new ArrayList<>();
      list.sort(String::compareToIgnoreCase);
      list.stream()
              .filter(item -> !item.isEmpty())     // założłem, że nowa lista nie ma zawierac pustych elementów
              .forEach(item -> sortedList.add(item));
      sortedList.forEach(System.out::println);
   }

   }



